package iss.java.part1;

/**
 * Created by WeiZehao on 16/10/16.
 * Part1 java图像处理
 * 输入输出的图像文件在image文件夹中
 * 实现了两种插值算法：二次线性插值算法和立方卷积插值算法
 * 分别输出了一个剪裁图片，一个缩小图片，多个放大图片，放大图片分别采用二次线性插值算法和三次卷积插值算法输出
 */

public class Main
{

    public static void main(String[] args)
    {
        /*测试剪裁图片*/
        CutImage cutImage = new CutImage("image/input/image_1_input.jpg",
                "image/output/image_1_output_cut.jpg",
                "jpg", 400, 50, 150, 200);
        try
        {
            cutImage.cut();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        /*测试放大图片*/
        ResizeImage resizeImage1 = new ResizeImage("image/input/image_2_input.jpg",
                "image/output/image_2_output_enlarge_Spline.jpg", "Spline", 1200, 1200);
        try
        {
            resizeImage1.resizeImage();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        /*测试缩小图片*/
        ResizeImage resizeImage2 = new ResizeImage("image/input/image_4_input.jpg",
                "image/output/image_4_output_shrink_Spline.jpg", "Spline", 960, 540);
        try
        {
            resizeImage2.resizeImage();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }
}
