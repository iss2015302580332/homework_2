package iss.java.part2;

/**
 * Created by WeiZehao on 16/10/17.
 * Part2 使用poi操作Excel
 * excel文件在document文件夹中
 */

public class Main
{
    public static void main(String[] args)
    {
        OperateExcel excel = new OperateExcel("document/inputExcel.xls");

        // 获得Excel表格
        excel.getExcel();
        // 读取当前表格信息
        excel.readExcelCell();
        // 合并总成绩列与其后一列
        excel.mergeCells();
        // 为总成绩一列设置求和公式
        excel.setSumFormula();
        // 设置学号一列的单元格格式为数值不保留小数
        excel.setCellFormat();
        // 使所有单元格水平居中对齐
        excel.changeAlign();
        // 输出新的表格
        excel.outputExcel();
    }
}
