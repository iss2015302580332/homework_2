package iss.java.part1;

import javax.imageio.ImageIO;
import javax.imageio.ImageReader;
import javax.imageio.stream.ImageInputStream;
import javax.imageio.stream.ImageOutputStream;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.util.Iterator;

/**
 * Created by WeiZehao on 16/10/14.
 */


public class CutImage
{
    private String _inputImagePath;     // 输入图片路径
    private String _outputImagePath;    // 输出图片路径
    private String _inputImgFormat;     // 输入图片格式
    private int _x;                     // 剪裁点x坐标
    private int _y;                     // 剪裁点y坐标
    private int _width;                 // 剪裁宽度
    private int _height;                // 剪裁高度

    public CutImage(String inputImagePath, String outputImagePath,
                    String inputImgFormat, int x, int y, int width, int height)
    {
        _inputImagePath = inputImagePath;
        _outputImagePath = outputImagePath;
        _inputImgFormat = inputImgFormat;
        _x = x;
        _y = y;
        _width = width;
        _height = height;
    }

    /*读取图片到内存*/
    private BufferedImage getBufferedImage(String inputPath, String inputFormat)
    {
        try
        {
            // 获取图片文件
            FileInputStream imgFile = new FileInputStream(inputPath);

            // 获取图片输入流
            ImageInputStream imgStream = ImageIO.createImageInputStream(imgFile);

            // 将read和输入源绑定
            Iterator<ImageReader> iterator = ImageIO.getImageReadersByFormatName(inputFormat);
            ImageReader imgReader = iterator.next();
            imgReader.setInput(imgStream, true);

            // 加载图片
            BufferedImage img = imgReader.read(0);
            return img;
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return null;
    }

    /*剪裁图片并输出*/
    public void cut() throws Exception
    {
        try
        {
            // 获取图片到内存
            BufferedImage img = getBufferedImage(_inputImagePath,_inputImgFormat);

            // 获取图片长宽信息
            int imgWidth = img.getWidth();
            int imgHeight = img.getHeight();

            int pixel = 0;
            // 越界检查
            if((_x + _width) > imgWidth || (_y + _height) > imgHeight)
            {
                throw new RuntimeException("out of bound!");
            }
            else
            {
                BufferedImage newImg = new BufferedImage(_width, _height, BufferedImage.TYPE_INT_RGB);
                /*
                 * 生成新的图片
                 * x1,y1是通过坐标获取旧图中的像素点
                 * x2,y2是通过坐标设置新图中的像素点
                 */
                for(int x1 = _x, x2 = 0; x1 < (_x + _width); x1++)
                {
                    for(int y1 = _y, y2 = 0; y1 < (_y + _height); y1++)
                    {
                        // 获取x1,y1处像素点的RGB值
                        pixel = img.getRGB(x1,y1);
                        // 设置x2,y2处像素点的RGB值
                        newImg.setRGB(x2, y2, pixel);
                        y2++;
                    }
                    x2++;
                }
                File outputImg = new File(_outputImagePath);
                ImageOutputStream outImgStream = ImageIO.createImageOutputStream(outputImg);

                // 输出图片
                ImageIO.write(newImg, "jpg", outImgStream);
            }// end else
        }// end try
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }// end method cut
}// end class CutImage
